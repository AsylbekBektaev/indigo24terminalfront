class Load{
    static Show(){
        let blockShow=document.querySelector('.block_LoadingPage');
        blockShow.style.display='block';
    }
    static noShow(){
        let blockShow=document.querySelector('.block_LoadingPage');
        blockShow.style.display='none';

    }
    static Anim2(){
        let circle=document.querySelector('.block_anim1'),
            circle2=document.querySelector('.block_anim2'),
            circle3=document.querySelector('.block_anim3'),
            check1=document.querySelector('.check_green1'),
            check2=document.querySelector('.check_green2'),
            block_anim=document.querySelector('.block_animation_Loading'),
            pro_block=document.querySelector('.block_pro'),
            load_block=document.querySelector('.block_LoadingPage'),
            text_loadBlock=document.querySelector('.block_text_LoadingPage');
        text_loadBlock.dataset.lang='processing_data';
        text_loadBlock.innerText='processing_data';
        circle.style.animationName='circle';
        circle2.style.animationName='circle2';
        circle3.style.animationName='circle3';

        circle.style.animationDuration='.8s';
        circle2.style.animationDuration='.8s';
        circle3.style.animationDuration='.8s';

        circle.style.animationDuration='.8s';
        circle2.style.animationDuration='.8s';
        circle3.style.animationDuration='.8s';

        circle.style.opacity='1';
        circle2.style.opacity='1';
        circle3.style.opacity='1';
        block_anim.style.backgroundColor='#BAD6F8';
        check1.style.opacity='0';
        check2.style.opacity='0';
    }
    static Anim(path){
        let circle=document.querySelector('.block_anim1'),
            circle2=document.querySelector('.block_anim2'),
            circle3=document.querySelector('.block_anim3'),
            check1=document.querySelector('.check_green1'),
            check2=document.querySelector('.check_green2'),
            block_anim=document.querySelector('.block_animation_Loading'),
            pro_block=document.querySelector('.block_pro'),
            load_block=document.querySelector('.block_LoadingPage'),
            text_loadBlock=document.querySelector('.block_text_LoadingPage');
        circle.style.animationName='del_anim1';
        circle2.style.animationName='del_anim2';
        circle3.style.animationName='del_anim3';

        circle.style.animationDuration='.8s';
        circle2.style.animationDuration='.8s';
        circle3.style.animationDuration='.8s';

        circle.style.animationDuration='.8s';
        circle2.style.animationDuration='.8s';
        circle3.style.animationDuration='.8s';
        setTimeout(()=>{
            circle.style.opacity='0';
            circle2.style.opacity='0';
            circle3.style.opacity='0';
            block_anim.style.backgroundColor='#E8FAE6';
            check1.style.opacity='1';
            check2.style.opacity='1';
        },770);

            setTimeout(()=>{
                text_loadBlock.dataset.lang='process';
                text_loadBlock.innerText='process';
            },1300);
            setTimeout(()=>{
                load_block.style.display='none';
            },2900);
            if(path){
                window.location=path;
            }

    }
}
